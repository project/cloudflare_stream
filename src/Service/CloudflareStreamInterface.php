<?php

namespace Drupal\cloudflare_stream\Service;

/**
 * Interface CloudflareStreamInterface.
 *
 * Describes the Cloudflare Stream service functions.
 */
interface CloudflareStreamInterface {

  /**
   * Whether or not the debug is enabled.
   *
   * @return bool
   *   True if a debug is enabled, false otherwise.
   */
  public function debugEnabled();

  /**
   * Returns the api_token from Cloudflare Stream configuration.
   *
   * @return string
   *   The API token.
   */
  public function getApiToken();

  /**
   * Returns the zone ID from Cloudflare Stream configuration.
   *
   * @return string
   *   The zone ID.
   */
  public function getCustomerSubdomain();

  /**
   * Returns the account ID from Cloudflare Stream configuration.
   *
   * @return string
   *   The account ID.
   */
  public function getAccountId();

  /**
   * Lists all allowed file extensions.
   *
   * @return string
   *   The allowed file extensions.
   */
  public function listAllowedFileExtensions();

}
