<?php

namespace Drupal\cloudflare_stream\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class CloudflareStream.
 *
 * The Cloudflare Stream service.
 */
class CloudflareStream implements CloudflareStreamInterface {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a CloudflareStream object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('cloudflare_stream.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function debugEnabled() {
    return $this->config->get('debug_messages');
  }

  /**
   * {@inheritDoc}
   */
  public function getApiToken() {
    return $this->config->get('api_token');
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomerSubdomain() {
    return $this->config->get('subdomain');
  }

  /**
   * {@inheritDoc}
   */
  public function getAccountId() {
    return $this->config->get('account_id');
  }

  /**
   * {@inheritDoc}
   */
  public function listAllowedFileExtensions() {
    return 'mp4 mkv mov avi flv ts tsv tsa mpg mpeg m2p ps mxf lxf gxf 3gp webm mpg qt';
  }

}
