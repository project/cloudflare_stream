<?php

namespace Drupal\cloudflare_stream\Service;

use Drupal\cloudflare_stream\DataStructure\TusUploadParameters;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for the CloudflareStream API service.
 */
interface CloudflareStreamApiInterface {

  /**
   * Upload a video using a single HTTP request.
   *
   * @param \Drupal\Core\Entity\EntityInterface $file
   *   The video file.
   */
  public function uploadVideoByHttpRequest(EntityInterface $file);

  /**
   * Initiates a TUS protocol upload.
   *
   * @param int $length
   *   Indicates the size of the entire upload in bytes.
   * @param string $creatorId
   *   (Optional, 64 character limit) An identifier for the media creator.
   * @param string $metaData
   *   (Optional) Comma-separated key-value pairs following the TUS protocol.
   *
   * @return \Drupal\cloudflare_stream\DataStructure\TusUploadParameters
   *   A URL to use for the upload.
   */
  public function initiateTusUpload(int $length, string $creatorId = '', string $metaData = ''): TusUploadParameters;

  /**
   * Get the embed code HTML for a video on Cloudflare Stream.
   *
   * @param string $identifier
   *   The identifier of the video.
   */
  public function getEmbedCodeHtml(string $identifier);

  /**
   * Delete a video on Cloudflare Stream.
   *
   * @param string $identifier
   *   The identifier of the video.
   *
   * @return bool
   *   Deletion was successful.
   */
  public function deleteVideo(string $identifier): bool;

  /**
   * List up to 1000 videos in one request.
   *
   * @param string|null $after
   *   Show videos created after this date-time.
   * @param string|null $before
   *   Show videos created before this time.
   * @param bool $include_counts
   *   Include stats in the response about the number of videos in response
   *   range and total number of videos available.
   * @param string|null $search
   *   A string provided in this field will be used to search over the 'name'
   *   key in meta field, which can be set with the upload request of after.
   * @param int|null $limit
   *   Number of videos to include in the response.
   * @param bool $asc
   *   List videos in ascending order of creation.
   * @param string|null $status
   *   Filter by statuses.
   */
  public function listVideos(string $after = NULL, string $before = NULL, bool $include_counts = FALSE, string $search = NULL, int $limit = NULL, bool $asc = FALSE, string $status = NULL);

  /**
   * Retrieve video info on Cloudflare Stream.
   *
   * @param string $identifier
   *   The identifier of the video.
   *
   * @return mixed[]
   *   Information about the video.
   */
  public function getDetails(string $identifier): array;

  /**
   * Validate the given token with the Cloudflare Stream API.
   *
   * @param string $token
   *   The token to validate.
   *
   * @return bool
   *   TRUE if the token is valid, FALSE otherwise.
   */
  public function validateToken(string $token);

}
