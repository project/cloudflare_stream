<?php

namespace Drupal\cloudflare_stream\Service;

use Drupal\cloudflare_stream\DataStructure\TusUploadParameters;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\TransferException;

/**
 * Provides a CloudflareStream API service class.
 */
class CloudflareStreamApi implements CloudflareStreamApiInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Cloudflare Stream service.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamInterface
   */
  protected $cloudflareStream;

  /**
   * Http timeout time.
   *
   * @var int
   */
  protected $timeout;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * CloudflareStreamApi constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   The HTTP client.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\cloudflare_stream\Service\CloudflareStreamInterface $cloudflareStream
   *   The Cloudflare Stream service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger channel factory.
   */
  public function __construct(
    Client $client,
    MessengerInterface $messenger,
    FileSystemInterface $fileSystem,
    CloudflareStreamInterface $cloudflareStream,
    LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->client = $client;
    $this->messenger = $messenger;
    $this->fileSystem = $fileSystem;
    $this->cloudflareStream = $cloudflareStream;
    $this->timeout = 600;
    $this->logger = $loggerFactory->get('cloudflare_stream');
  }

  /**
   * {@inheritdoc}
   */
  public function uploadVideoByHttpRequest(EntityInterface $file) {
    $fileRealPath = $this->fileSystem->realpath($file->getFileUri());
    $filename = $file->getFilename();

    try {
      $response = $this->client->post($this->getApiUrl(), [
        'timeout' => $this->getTimeout(),
        'headers' => $this->getHeaders(),
        'multipart' => [
          [
            'name' => 'file',
            'contents' => file_get_contents($fileRealPath),
            'filename' => $filename,
          ],
        ],
      ]);

      return Json::decode($response->getBody()->getContents());
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function initiateTusUpload(int $length, string $creatorId = '', string $metaData = ''): TusUploadParameters {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';
      $headers['Tus-Resumable'] = '1.0.0';
      $headers['Upload-Length'] = $length;
      if ($creatorId !== '') {
        $headers['Upload-Creator'] = $creatorId;
      }
      if ($metaData !== '') {
        $headers['Upload-Metadata'] = $metaData;
      }
      $response = $this->client->post($this->getApiUrl(), [
        'headers' => $headers,
      ]);
      $urls = $response->getHeader('location');
      $ids = $response->getHeader('stream-media-id');
      if (count($urls) !== 1) {
        $count = count($urls);
        throw new TransferException("Location header should contain 1 value, actually contains $count");
      }
      if (count($ids) !== 1) {
        $count = count($ids);
        throw new TransferException("Stream-media-id header should contain 1 value, actually contains $count");
      }
      return new TusUploadParameters(TRUE, reset($urls), reset($ids));
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
      return new TusUploadParameters(FALSE, '', '');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEmbedCodeHtml(string $identifier) {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';

      return $this->client->get("{$this->getApiUrl()}/$identifier/embed", [
        'headers' => $headers,
      ]);

    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVideo(string $identifier): bool {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';

      $response = $this->client->delete("{$this->getApiUrl()}/$identifier", [
        'headers' => $headers,
      ]);
      return $response->getStatusCode() === 200;
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDetails(string $identifier): array {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';

      $response = $this->client->get("{$this->getApiUrl()}/$identifier", [
        'headers' => $headers,
      ]);

      $data = Json::decode($response->getBody()->getContents());

      if ($data['success']) {
        return $data['result'] ?? [];
      }
      else {
        return [];
      }
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function listVideos(string $after = NULL, string $before = NULL, bool $include_counts = FALSE, string $search = NULL, int $limit = NULL, bool $asc = FALSE, string $status = NULL) {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';
      $queryParameters = [];
      $allowedQueryParameters = [
        'after',
        'before',
        'include_counts',
        'search',
        'limit',
        'asc',
        'status',
      ];

      foreach ($allowedQueryParameters as $key) {
        if (${$key}) {
          $queryParameters[$key] = ${$key};
        }
      }

      $response = $this->client->get("{$this->getApiUrl()}", [
        'headers' => $headers,
        'query' => $queryParameters,
      ]);

      return Json::decode($response->getBody()->getContents());
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateToken($token) {
    try {
      $response = $this->client->get('https://api.cloudflare.com/client/v4/user/tokens/verify', [
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => 'Bearer ' . $token,
        ],
      ]);

      $result = Json::decode($response->getBody()->getContents());

      if ($result['success'] && $result['result']['status'] === 'active') {
        return TRUE;
      }

    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }

    return FALSE;
  }

  /**
   * Get the API URL.
   *
   * @return string
   *   The URL.
   */
  protected function getApiUrl() {
    return "https://api.cloudflare.com/client/v4/accounts/{$this->cloudflareStream->getAccountId()}/stream";
  }

  /**
   * Return the headers for the API request.
   */
  protected function getHeaders() {
    return [
      'Authorization' => 'Bearer ' . $this->cloudflareStream->getApiToken(),
    ];
  }

  /**
   * Return the timeout time.
   */
  protected function getTimeout() {
    return $this->timeout;
  }

  /**
   * Handles the error message.
   *
   * Using FormattableMarkup allows for the use of <pre/> tags, giving a more
   * readable log item.
   *
   * @param \GuzzleHttp\Exception\GuzzleException $error
   *   The error.
   * @param string $function
   *   The function that calls the function.
   */
  protected function handleError(GuzzleException $error, string $function) {
    // Get the original response.
    $response = $error->getResponse();

    // Get the info returned from the remote server.
    $response_info = Json::decode($response->getBody()->getContents());

    // Log the error.
    $this->logger->error('@class (@function): API connection error. Error details are as follows:<pre>@response</pre>',
      [
        '@class' => get_class(),
        '@function' => $function,
        '@response' => print_r($response_info['errors'], TRUE),
      ]
    );

    // Show the error in the frontend if debug message option is enabled.
    if ($this->cloudflareStream->debugEnabled()) {
      $this->messenger->addError(new FormattableMarkup(
        '@class (@function): API connection error. Error details are as follows:<pre>@response</pre>',
        [
          '@class' => get_class(),
          '@function' => $function,
          '@response' => print_r($response_info['errors'], TRUE),
        ]
      ));
    }
  }

}
