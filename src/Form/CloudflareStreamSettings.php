<?php

namespace Drupal\cloudflare_stream\Form;

use Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures the settings for Cloudflare Stream module.
 */
class CloudflareStreamSettings extends ConfigFormBase {

  /**
   * The Cloudflare Stream API.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface
   */
  protected $cloudflareStreamApi;

  /**
   * Constructs a new CloudflareStreamSettings.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface $cloudflare_stream_api
   *   The Cloudflare Stream API service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, CloudflareStreamApiInterface $cloudflare_stream_api) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->cloudflareStreamApi = $cloudflare_stream_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('cloudflare_stream.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudflare_stream.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_stream_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Getting already saved config from the database.
    $api_token = $this->config('cloudflare_stream.settings')->get('api_token');
    $account_id = $this->config('cloudflare_stream.settings')->get('account_id');
    $subdomain = $this->config('cloudflare_stream.settings')->get('subdomain');
    $debug_messages = $this->config('cloudflare_stream.settings')->get('debug_messages');

    // Adding form fields to the form.
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#description' => $this->t('To find your API Token go to the Cloudflare Dashboard, optionally select your account, and select the website. In the sidebar API section you can find a link to the API Token page where you can generate an API token.'),
      '#default_value' => $api_token,
      '#required' => TRUE,
    ];
    $form['account_specific_settings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Account specific settings'),
      'instruction' => [
        '#markup' => '<p>' . $this->t('The Account & Customer Subdomain are account specific. To find these browse to the Cloudflare Dashboard, select an account if you have multiple. Select <em>Stream</em> in the left sidebar. The needed info can be found under the <em>Account details</em> heading in the right sidebar.') . "</p>",
      ],
    ];
    $form['account_specific_settings']['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#default_value' => $account_id,
      '#required' => TRUE,
    ];
    $form['account_specific_settings']['subdomain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer subdomain'),
      '#default_value' => $subdomain,
      '#description' => $this->t('Input only the first subdomain.  If Cloudflare displays <code>customer-abc1234.cloudflarestream.com</code> then you should enter <code>customer-abc1234</code>'),
      '#required' => TRUE,
    ];
    $form['debug_messages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show debug messages'),
      '#description' => $this->t("Check this box if you want to show debug messages in the frontend. By default, we only log the error messages in watchdog."),
      '#default_value' => $debug_messages,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_token = $form_state->getValue('api_token');

    if (!$this->cloudflareStreamApi->validateToken($api_token)) {
      $form_state->setErrorByName('api_token', $this->t('The API Token is invalid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Trimming user entry for any whitespace.
    $api_token = preg_replace('/\s/', '', $form_state->getValue('api_token'));
    $account_id = preg_replace('/\s/', '', $form_state->getValue('account_id'));
    $subdomain = preg_replace('/\s/', '', $form_state->getValue('subdomain'));
    $debug_messages = $form_state->getValue('debug_messages');

    // Saving config in the database.
    $this->config('cloudflare_stream.settings')
      ->set('api_token', $api_token)
      ->set('account_id', $account_id)
      ->set('subdomain', $subdomain)
      ->set('debug_messages', $debug_messages)
      ->save();
    $this->messenger()->addMessage($this->t('Cloudflare configurations saved successfully.'));
  }

}
