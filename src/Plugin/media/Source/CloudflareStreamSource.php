<?php

namespace Drupal\cloudflare_stream\Plugin\media\Source;

use Drupal\cloudflare_stream\Service\CloudflareStreamInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\media\Attribute\MediaSource;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A media source plugin for Cloudflare Stream assets.
 *
 * @see \Drupal\file\FileInterface
 */
#[MediaSource(
  id: 'cloudflare_stream',
  label: new TranslatableMarkup('Cloudflare Stream'),
  description: new TranslatableMarkup('A media source plugin for Cloudflare Stream assets.'),
  allowed_field_types: ["cloudflarevideo"],
  default_thumbnail_filename: 'video.png'
)]
class CloudflareStreamSource extends File {

  /**
   * The Cloudflare Stream service.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamInterface
   */
  protected $cloudflareStream;

  /**
   * Constructs a HostedVideoSource instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\cloudflare_stream\Service\CloudflareStreamInterface $cloudflare_stream
   *   The Cloudflare Stream service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    CloudflareStreamInterface $cloudflare_stream,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->cloudflareStream = $cloudflare_stream;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('cloudflare_stream')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set(
      'settings',
      [
        'file_extensions' => $this->cloudflareStream->listAllowedFileExtensions(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'cloudflarevideo_default',
      'label' => 'visually_hidden',
    ]);
  }

}
