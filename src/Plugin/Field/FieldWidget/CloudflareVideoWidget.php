<?php

namespace Drupal\cloudflare_stream\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the Cloudflare video widget.
 *
 * @FieldWidget(
 *   id = "cloudflarevideo_default",
 *   module = "cloudflare_stream",
 *   label = @Translation("Cloudflare Video"),
 *   field_types = {
 *     "cloudflarevideo"
 *   }
 * )
 */
class CloudflareVideoWidget extends FileWidget {

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $element = parent::process($element, $form_state, $form);

    // Add own description to the description field.
    if (isset($element['description']['#description'])) {
      $element['description']['#description'] = t('The description will be shown below the video.');
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function value($element, $input, FormStateInterface $form_state) {
    $return = parent::value($element, $input, $form_state);
    $return['cloudflareStreamVideoID'] = $element['#default_value']['cloudflareStreamVideoID'] ?? NULL;
    $return['thumbnail'] = $element['#default_value']['thumbnail'] ?? NULL;

    return $return;
  }

}
