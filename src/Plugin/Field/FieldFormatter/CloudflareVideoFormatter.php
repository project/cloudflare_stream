<?php

namespace Drupal\cloudflare_stream\Plugin\Field\FieldFormatter;

use Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface;
use Drupal\cloudflare_stream\Service\CloudflareStreamInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'cloudflarevideo_default' formatter.
 *
 * @FieldFormatter(
 *   id = "cloudflarevideo_default",
 *   module = "cloudflare_stream",
 *   label = @Translation("Cloudflare Video"),
 *   field_types = {
 *     "cloudflarevideo"
 *   }
 * )
 */
class CloudflareVideoFormatter extends FormatterBase {

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The Cloudflare Stream service.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamInterface
   */
  protected $cloudflareStream;

  /**
   * The Cloudflare Stream API service.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface
   */
  protected $cloudflareStreamApi;

  /**
   * Constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid.
   * @param \Drupal\cloudflare_stream\Service\CloudflareStreamInterface $cloudflare_stream
   *   The Cloudflare Stream service.
   * @param \Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface $cloudflare_stream_api
   *   The Cloudflare Stream API service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    UuidInterface $uuid,
    CloudflareStreamInterface $cloudflare_stream,
    CloudflareStreamApiInterface $cloudflare_stream_api,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->uuid = $uuid;
    $this->cloudflareStream = $cloudflare_stream;
    $this->cloudflareStreamApi = $cloudflare_stream_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('uuid'),
      $container->get('cloudflare_stream'),
      $container->get('cloudflare_stream.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => TRUE,
      'muted' => '',
      'autoplay' => TRUE,
      'loop' => FALSE,
      'dimensions' => [
        'width' => '854',
        'height' => '480',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['controls'] = [
      '#title' => $this->t('Controls'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show the controls on the Cloudflare videos'),
      '#default_value' => $this->getSetting('controls'),
    ];
    $elements['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#description' => $this->t('Autoplay the Cloudflare videos'),
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $elements['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#description' => $this->t('Loop the Cloudflare videos'),
      '#default_value' => $this->getSetting('loop'),
    ];
    $elements['muted'] = [
      '#title' => $this->t('Muted Video'),
      '#type' => 'checkbox',
      '#description' => $this->t("Mute the Cloudflare videos."),
      '#default_value' => $this->getSetting('muted'),
    ];
    $elements['dimensions'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Video dimensions'),
      'instruction' => [
        '#markup' => "<p>" . $this->t("By default we set the height & width of the video in pixels. If the width & height fields are left empty we'll fallback to a 100% width & 100% height value.") . "</p>",
      ],
    ];
    $elements['dimensions']['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'number',
      '#field_suffix' => 'px',
      '#default_value' => $this->getSetting('dimensions')['width'],
      '#required' => FALSE,
      '#size' => 20,
      '#element_validate' => [[static::class, 'validateDimensionFormElement']],
    ];
    $elements['dimensions']['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'number',
      '#field_suffix' => 'px',
      '#default_value' => $this->getSetting('dimensions')['height'],
      '#required' => FALSE,
      '#size' => 20,
      '#element_validate' => [[static::class, 'validateDimensionFormElement']],
    ];
    return $elements;
  }

  /**
   * Form element validation handler for dimension form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateDimensionFormElement(array &$element, FormStateInterface $form_state) {
    // Return empty value when input is 0.
    if ($form_state->getValue($element['#parents']) === "0") {
      $form_state->setValue($element['#parents'], '');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $width = $this->getSetting('dimensions')['width'];
    $height = $this->getSetting('dimensions')['height'];
    $dimensions = $this->t('@width x @height', [
      '@width' => $width ?: '100%',
      '@height' => $height ?: '100%',
    ]);
    $summary[] = $this->t(
      'The Cloudflare video player is:<br>- @muted<br>- @dimensions<br>- @autoplay<br>- @controls<br>- @loop', [
        '@muted' => $this->getSetting('muted') ? $this->t('muted') : $this->t('unmuted'),
        '@dimensions' => $dimensions,
        '@autoplay' => $this->getSetting('autoplay') ? $this->t('autoplaying') : $this->t('not autoplaying'),
        '@controls' => $this->getSetting('controls') ? $this->t('showing controls') : $this->t('hiding controls'),
        '@loop' => $this->getSetting('loop') ? $this->t('looping') : $this->t('not looping'),
      ]
    );
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $drupalSettings = [];
    foreach ($items as $delta => $item) {
      $cloudflareVideoID = $this->getVideoId($item);

      if ($cloudflareVideoID) {
        $request = $this->cloudflareStreamApi->getEmbedCodeHtml($cloudflareVideoID);

        if ($request) {
          $elements[$delta] = [
            '#type' => 'container',
            '#attributes' => [
              'id' => $unique_stream_id = $this->uuid->generate(),
            ],
            '#theme' => 'cloudflare_video',
            '#stream' => $request->getBody()->getContents(),
            '#description' => $item->get('description')->getValue(),
          ];

          $drupalSettings[$unique_stream_id] = [
            'unique_stream_id' => $unique_stream_id,
            'width' => $this->getSetting('dimensions')['width'] ? $this->getSetting('dimensions')['width'] . 'px' : '100%',
            'height' => $this->getSetting('dimensions')['height'] ? $this->getSetting('dimensions')['height'] . 'px' : '100%',
            'controls' => $this->getSetting('controls') ? 'controls' : '',
            'autoplay' => $this->getSetting('autoplay') ? 'autoplay' : '',
            'loop' => $this->getSetting('loop') ? 'loop' : '',
            'muted' => $this->getSetting('muted') ? 'muted' : '',
          ];
        }
      }

    }

    // Attach library & Cloudflare Video settings if videos could be loaded.
    if (!empty($elements)) {
      $elements['#attached'] = [
        'library' => [
          'cloudflare_stream/cloudflare_stream',
        ],
        'drupalSettings' => [
          'cloudflare_stream' => $drupalSettings,
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = $this->viewElements($items, $langcode);
    if (empty($elements)) {
      return [];
    }
    return parent::view($items, $langcode);
  }

  /**
   * Returns the video id on cloudflare stream.
   */
  protected function getVideoId($item) {
    $videoID = $item->get('cloudflareStreamVideoID')->getValue();

    return $videoID;
  }

}
