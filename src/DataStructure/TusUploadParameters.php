<?php

declare(strict_types=1);

namespace Drupal\cloudflare_stream\DataStructure;

/**
 * An immutable value map class with readonly properties.
 */
final class TusUploadParameters {

  /**
   * Store the parameter values.
   */
  public function __construct(
    public readonly bool $ready,
    public readonly string $destinationUrl,
    public readonly string $videoId,
  ) {}

}
