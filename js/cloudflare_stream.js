(($, Drupal, drupalSettings) => {
  Drupal.behaviors.cloudflareStream = {
    attach() {
      const uniqueStreamIDs = drupalSettings.cloudflare_stream;

      // Loop through all Cloudflare Streams.
      $.each(uniqueStreamIDs, (key, settings) => {
        const stream = $(`#${key}> stream`);
        const { muted } = settings;
        const { controls } = settings;
        const { autoplay } = settings;
        const { loop } = settings;
        const { width } = settings;
        const { height } = settings;

        // Adding attributes to stream video.
        stream.attr({ preload: '' });
        if (controls) {
          stream.prop('controls', true);
          $('.video-js')
            .removeClass('vjs-controls-disabled')
            .addClass('vjs-controls-enabled');
        }
        if (muted) {
          stream.attr('muted', true);
        }
        if (autoplay) {
          stream.prop('autoplay', true);
        }
        if (loop) {
          stream.prop('loop', true);
        }
        stream.css('width', width);
        stream.css('height', height);
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
