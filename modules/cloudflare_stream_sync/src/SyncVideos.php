<?php

namespace Drupal\cloudflare_stream_sync;

use Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface;
use Drupal\cloudflare_stream\Service\CloudflareStreamInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;

/**
 * Sync videos service.
 *
 * @package Drupal\cloudflare_sync
 */
class SyncVideos {

  /**
   * The Cloudflare Stream Sync config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected DateFormatterInterface $dateFormatter,
    protected CloudflareStreamInterface $cloudflareStream,
    protected CloudflareStreamApiInterface $cloudflareStreamApi,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->config = $config_factory->getEditable('cloudflare_stream_sync.settings');
  }

  /**
   * Fetch videos from the Cloudflare Stream with an after query parameter.
   *
   * @return array
   *   An array with the result.
   */
  public function fetchVideos(): array {
    $date = NULL;

    // Get last imported timestamp to pass to the listVideos function.
    $timestamp = $this->config->get('last_imported');
    if (isset($timestamp)) {
      $date = $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d\TH:m:s\Z', 'UTC');
    }

    // Get videos from Cloudflare Stream.
    $response = $this->cloudflareStreamApi->listVideos($date);

    if (is_array($response['result'])) {
      return $response['result'];
    }
    return [];
  }

  /**
   * Sync the external videos.
   *
   * If a media type id is provided, it is used in place of the value
   * stored in config.
   *
   * @param string|null $media_type_id
   *   A media type id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function syncVideos(?string $media_type_id = NULL) {
    $media_type_id = $media_type_id ?? $this->config->get('media_type_id') ?? '';
    if ($media_type_id === '') {
      throw new \ValueError('A media type ID was not provided and none is stored in cloudflare_stream_sync.settings');
    }
    $media_type = $this->entityTypeManager->getStorage('media_type')->load($media_type_id);
    if (!($media_type instanceof MediaType)) {
      throw new \TypeError('Media type ' . $media_type_id . ' does not exist.');
    }

    // Fetch new videos.
    $video_list = $this->fetchVideos();
    if (empty($video_list)) {
      return;
    }

    // Process videos.
    foreach ($video_list as $video) {
      SyncVideos::processVideo($video, $media_type);
    }

    // Update last imported timestamp.
    $this->config->set('last_imported', time())->save();
  }

  /**
   * Process the external video.
   *
   * @param array $video
   *   The external video information.
   * @param \Drupal\media\Entity\MediaType $mediaType
   *   The media type(bundle).
   *
   * @return false|string
   *   False if no video is found, otherwise the filename.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processVideo(array $video, MediaType $mediaType) {
    // Get video ID.
    $videoID = SyncVideos::getVideoId($video);
    $mediaSource = $mediaType->getSource();
    $sourceField = $mediaSource->getSourceFieldDefinition($mediaType);

    if (!SyncVideos::checkIfVideoIdExists($videoID, $sourceField)) {
      // Get filename.
      $filename = SyncVideos::getFilename($video);

      // Create an empty file.
      $file = \Drupal::service('file.repository')->writeData('', 'public://' . $filename);

      // Create a media object of the file.
      $media = Media::create([
        'bundle' => $mediaType->id(),
        'uid' => \Drupal::currentUser()->id(),
        'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
        'name' => $filename,
        $sourceField->getName() => [
          'target_id' => $file->id(),
          'cloudflareStreamVideoID' => $videoID,
          'thumbnail' => SyncVideos::getThumbnail($video),
        ],
      ]);
      $media->setPublished(TRUE);
      $media->save();

      // Register file usages in the DB.
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'file', 'media', $media->id());
      $file_usage->add($file, 'cloudflare_stream', 'cloudflarevideo', \Drupal::currentUser()
        ->id());
      $file->setPermanent();
      $file->save();

      return $filename;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Batch API syncVideoCallback function.
   *
   * Helper function to sync the video.
   */
  public static function syncVideoCallback(array $video, MediaType $mediaType, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
    }

    if ($filename = SyncVideos::processVideo($video, $mediaType)) {
      $context['sandbox']['progress']++;
      $context['results'][] = $filename;
      $context['message'] = t('Syncing video @filename', [
        '@filename' => $filename,
      ]);
    }
  }

  /**
   * Batch API finishedCallback function.
   *
   * Finished batch callback.
   */
  public static function finishedCallback($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      // Set last imported timestamp.
      $config = \Drupal::service('config.factory')
        ->getEditable('cloudflare_stream_sync.settings');
      $config->set('last_imported', time())->save();

      $message = \Drupal::translation()->formatPlural(
        count($results),
        t("One video processed."), t("@count videos processed.")
      );
      $messenger->addMessage($message);
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        t("An error occurred while processing @operation with arguments : @args",
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * Return the filename from the video.
   *
   * @param array $video
   *   The external video information.
   *
   * @return string
   *   The filename.
   */
  protected static function getFilename(array $video) {
    // Borrow a bit of logic from \FileEventSubscriber::sanitizeFilename().
    $filename = preg_replace('/\s/u', '-', trim($video['meta']['name']));
    $filename = preg_replace('/[^0-9A-Za-z_.-]/u', '-', $filename);
    $filename = mb_strtolower($filename);
    return $filename;
  }

  /**
   * Return the video ID from the video.
   *
   * @param array $video
   *   The external video information.
   *
   * @return string
   *   The video ID.
   */
  protected static function getVideoId(array $video) {
    return $video['uid'];
  }

  /**
   * Return the thumbnail from the video.
   *
   * @param array $video
   *   The external video information.
   *
   * @return string
   *   The thumbnail uri.
   */
  protected static function getThumbnail(array $video) {
    return $video['thumbnail'];
  }

  /**
   * Check if VideoID already exists.
   *
   * @param string $videoID
   *   The video ID.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The cloudflare_stream field definition on the media bundle.
   *
   * @return bool
   *   True if video exists, false otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function checkIfVideoIdExists(string $videoID, FieldDefinitionInterface $fieldDefinition) {
    $fieldName = $fieldDefinition->getName();
    $query = \Drupal::entityTypeManager()->getStorage('media')->getQuery();
    $query->condition("$fieldName.cloudflareStreamVideoID", $videoID);
    $results = $query->accessCheck(FALSE)->execute();

    if (count($results) === 0) {
      return FALSE;
    }

    return TRUE;
  }

}
