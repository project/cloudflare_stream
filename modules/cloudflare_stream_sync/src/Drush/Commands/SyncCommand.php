<?php

namespace Drupal\cloudflare_stream_sync\Drush\Commands;

use Drupal\cloudflare_stream_sync\SyncVideos;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The drush sync command.
 */
final class SyncCommand extends DrushCommands {

  /**
   * Constructs the command.
   */
  public function __construct(
    private readonly SyncVideos $sync,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cloudflare_stream_sync'),
    );
  }

  /**
   * Perform the Cloudflare Stream video sync.
   */
  #[CLI\Command(name: 'cloudflarestream:sync', aliases: ['css'])]
  #[CLI\Argument(name: 'media_type_id', description: 'The media type to receive the sync.')]
  #[CLI\Usage(name: 'cloudflarestream:sync', description: 'Sync using the media type ID stored in Cloudflare Stream Sync config.')]
  #[CLI\Usage(name: 'cloudflarestream:sync my_media_type', description: 'Sync using `my_media_type` as the media to receive the sync.')]
  public function import($media_type_id = NULL): void {
    try {
      $this->sync->syncVideos($media_type_id);
    }
    catch (\Exception $exception) {
      // Log the error.
      $this->logger()->error(dt('@class (@function): sync error. Error details are as follows:<pre>@response</pre>',
        [
          '@class' => get_class(),
          '@function' => __FUNCTION__,
          '@response' => print_r($exception->getMessage(), TRUE),
        ]
      ));
    }
  }

}
