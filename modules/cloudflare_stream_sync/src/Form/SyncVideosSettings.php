<?php

namespace Drupal\cloudflare_stream_sync\Form;

use Drupal\cloudflare_stream_sync\SyncVideos;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for configuring the settings for the Cloudflare Stream Sync module.
 */
class SyncVideosSettings extends FormBase {

  /**
   * The Cloudflare Stream Sync config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected DateFormatterInterface $dateFormatter,
    protected SyncVideos $syncVideos,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->config = $config_factory->getEditable('cloudflare_stream_sync.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('cloudflare_stream_sync'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_stream_sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $mediaTypes = $this->entityTypeManager->getStorage('media_type')->loadByProperties(['source' => 'cloudflare_stream']);
    $types = [];
    foreach ($mediaTypes as $type) {
      $types[$type->id()] = $type->label();
    }
    // Explanation.
    $form['explanation'] = [
      '#markup' => $this->t("<p>The <strong>Cloudflare Stream Sync</strong> module creates a media item for each video item that is on the Cloudflare stream platform.</p>
<ul><li>The <strong>initial sync</strong> will sync all videos present on Cloudflare.</br>
<p>During that sync, we check whether the video exists as a media item on the website.</br>
If it doesn't, it will be imported.</p></li>
<li>The <strong>next syncs</strong> will only import new videos that were added to the Cloudflare Stream platform <strong>after</strong> the last sync date.</li></ul>"),
    ];

    $form['media_type_id'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select Media Type'),
      '#options' => $types,
      '#empty_value' => '',
      '#default_value' => $this->config->get('media_type_id') ?? NULL,
    ];

    // Show last imported timestamp as date eg. 2014-01-02T02:20:00Z
    // Set last imported timestamp.
    $timestamp = $this->config->get('last_imported');
    if (isset($timestamp)) {
      $date = $this->dateFormatter->format($timestamp, 'short');
      $form['last_imported_timestamp'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("<strong>Last import:</strong> @date", ['@date' => $date]),
      ];
    }

    // Submit button.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync videos'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $media_type_id = $form_state->getValue('media_type_id');
    $this->config->set('media_type_id', $media_type_id);
    $this->config->save();

    $media_type = $this->entityTypeManager->getStorage('media_type')->load($media_type_id);

    $batch = new BatchBuilder();
    $batch->setTitle($this->t("Synchronization process"))
      ->setInitMessage($this->t("The sync process is starting."))
      ->setProgressMessage($this->t("Processed @current out of @total."))
      ->setErrorMessage($this->t("The sync has encountered an error."))
      ->setFinishCallback([SyncVideos::class, 'finishedCallback']);

    $videos = $this->syncVideos->fetchVideos();
    if (count($videos) >= 1) {
      foreach ($videos as $video) {
        $batch->addOperation([SyncVideos::class, 'syncVideoCallback'], [$video, $media_type]);
      }

      batch_set($batch->toArray());
    }
    else {
      $this->messenger()->addMessage($this->t('Nothing to sync.'));
    }
  }

}
