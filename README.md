CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Troubleshooting
 * Project members


INTRODUCTION
------------

Cloudflare Stream is an easy-to-use, affordable, on-demand video streaming
platform. Stream seamlessly integrates video storage, encoding, and a
customizable player with Cloudflare’s fast, secure, and reliable global network,
so that you can spend less time managing video delivery and more time building
and promoting your product.

This modules integrates Cloudflare Stream as a new field which lets you upload
video in almost any format and stream high quality video on any device without
having to build your own streaming solution from scratch.


REQUIREMENTS
------------

A Cloudflare account with the "Stream" package enabled.
Sign-up on https://dash.cloudflare.com/sign-up


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Configure the Cloudflare Stream settings in Administration » Config » Media »
Cloudflare Stream


USAGE
-------------

Add the Cloudflare Video field to any node or entity, configure as any other
field in your Drupal site. Once you upload a video, a copy of the video will be
uploaded to Cloudflare Stream and videos will be streamed via their service.
By default the original video is kept in Drupal so you can easily change service
without having to worry about data loss. Nevertheless, there is an option to
not store the files locally.

Multiple display options are available to mute, loop, autoplay,... the video.

Install the Cloudflare Stream hosted video submodule in case you want to use
Cloudflare videos as a media entity.


TROUBLESHOOTING
-------------

* If there is an unexpected issue, please check the issue queue:
  https://www.drupal.org/project/issues/cloudflare_stream

* If there is an undocumented issue, please create one:
  https://www.drupal.org/node/add/project-issue/cloudflare_stream


PROJECT MEMBERS
-----------

See the list of current project members on
https://git.drupalcode.org/project/cloudflare_stream/-/project_members
