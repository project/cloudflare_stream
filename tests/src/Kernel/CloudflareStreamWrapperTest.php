<?php

declare(strict_types=1);

namespace Drupal\Tests\cloudflare_stream\Kernel;

use Drupal\cloudflare_stream\DataStructure\TusUploadParameters;
use Drupal\cloudflare_stream\Service\CloudflareStream;
use Drupal\cloudflare_stream\Service\CloudflareStreamApi;
use Drupal\cloudflare_stream\StreamWrapper\CloudflareStreamWrapper;
use Drupal\cloudflare_stream\Tus\UploadClient;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\KernelTests\KernelTestBase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Test the custom methods of our extended wrapper class used in uploads.
 *
 * Methods to test are:
 * - stream_stat()
 * - isRemoteFileId()
 * - stream_close()
 * - stream_metadata()
 *
 * All other methods called in an upload are passed to the LocalStream class.
 *
 * @coversDefaultClass \Drupal\cloudflare_stream\StreamWrapper\CloudflareStreamWrapper
 *
 * @group cloudflare_stream
 */
final class CloudflareStreamWrapperTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['cloudflare_stream'];

  /**
   * Use to copy file into the test stream.
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Mocked service object.
   */
  protected CloudflareStream $cloudflareStream;

  /**
   * Mocked service object.
   */
  protected CloudflareStreamApi $cloudflareStreamApi;

  /**
   * Mocked service object.
   */
  protected PrivateTempStoreFactory $tempStoreFactory;

  /**
   * Mocked service object.
   */
  protected UploadClient|MockObject $uploadClient;

  /**
   * Object under test.
   */
  protected CloudflareStreamWrapper $wrapperObject;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->fileSystem = $this->container->get('file_system');
    $this->cloudflareStream = $this->getMockBuilder('Drupal\cloudflare_stream\Service\CloudflareStream')
      ->disableOriginalConstructor()->getMock();
    $this->cloudflareStreamApi = $this->getMockBuilder('Drupal\cloudflare_stream\Service\CloudflareStreamApi')
      ->disableOriginalConstructor()->getMock();
    $this->tempStoreFactory = $this->container->get('tempstore.private');
    $this->uploadClient = $this->createMock('\Drupal\cloudflare_stream\Tus\UploadClient');
    $this->wrapperObject = new CloudflareStreamWrapper(
      $this->cloudflareStream,
      $this->cloudflareStreamApi,
      $this->tempStoreFactory,
      $this->uploadClient
    );
  }

  /**
   * Test stream_stat.
   *
   * @covers ::stream_stat
   */
  public function testStreamStatRoot(): void {
    $this->cloudflareStreamApi
      ->expects($this->never())
      ->method('getDetails');
    $ignored = '';
    $this->wrapperObject->stream_open('cfstream://', 'r', 0, $ignored);
    $stat = $this->wrapperObject->stream_stat();
    $this->assertIsArray($stat);
    $this->assertCount(26, $stat);
    // The returned mode of the `/tmp` directory is 17407.
    $this->assertEquals(17407, $stat['mode']);
  }

  /**
   * Test stream_stat.
   *
   * @covers ::stream_stat
   */
  public function testStreamStatRemote(): void {
    $this->cloudflareStreamApi
      ->expects($this->exactly(2))
      ->method('getDetails')
      ->with('1234')
      ->willReturn(['size' => 222]);
    $ignored = '';
    $this->wrapperObject->stream_open('cfstream://1234', 'r', 0, $ignored);
    $stat = $this->wrapperObject->stream_stat();
    $this->assertIsArray($stat);
    $this->assertCount(26, $stat);
    // Size returned by the API call should now be in the stat array as 'size'.
    $this->assertEquals(222, $stat['size']);
  }

  /**
   * Test stream_stat.
   *
   * @covers ::stream_stat
   */
  public function testStreamStatLocal(): void {
    $testFilePath = dirname(__DIR__, 2) . '/data/test-data.gif';
    $this->fileSystem->copy($testFilePath, '/tmp/test-data.gif');
    $this->cloudflareStreamApi
      ->expects($this->exactly(2))
      ->method('getDetails')
      ->with('test-data.gif')
      ->willReturn([]);
    $ignored = '';
    $this->wrapperObject->stream_open('cfstream://test-data.gif', 'r', 0, $ignored);
    $stat = $this->wrapperObject->stream_stat();
    $this->assertIsArray($stat);
    $this->assertCount(26, $stat);
    // Size should now the test file size.
    $this->assertEquals(1972, $stat['size']);
  }

  /**
   * Test closing the stream.
   *
   * @covers ::stream_open
   * @covers ::stream_close
   */
  public function testStreamClose(): void {
    $testFilePath = dirname(__DIR__, 2) . '/data/test-data.gif';
    $this->fileSystem->copy($testFilePath, '/tmp/test-data.gif');
    $parameters = new TusUploadParameters(TRUE, 'https://cloudflare.example.com/files', 'test-video-id');
    $this->cloudflareStreamApi
      ->expects($this->once())
      ->method('initiateTusUpload')
      ->willReturn($parameters);
    $this->uploadClient
      ->expects($this->once())
      ->method('upload');
    $ignored = '';
    $this->wrapperObject->stream_open('cfstream://test-data.gif', 'wb', 0, $ignored);
    $result = $this->wrapperObject->stream_close();
    $this->assertTrue($result);
    $videoId = $this->tempStoreFactory->get('cloudflare_stream')->get('cfstream://test-data.gif');
    $this->assertEquals('test-video-id', $videoId);
  }

  /**
   * Test metadata.
   *
   * @covers ::stream_metadata
   */
  public function testStreamMetadata(): void {
    // If the file is not remote pass the work to the core class.
    // Therefore only test a non-existent path.
    // The only logically active parameter is the file path.
    $this->cloudflareStreamApi
      ->expects($this->once())
      ->method('getDetails')
      ->with('remote-id')
      ->willReturn(['size' => 222]);
    $result = $this->wrapperObject->stream_metadata('cfstream://remote-id', STREAM_META_ACCESS, '755');
    $this->assertFalse($result);
  }

}
