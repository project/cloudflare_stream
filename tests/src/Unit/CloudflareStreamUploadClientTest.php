<?php

declare(strict_types=1);

namespace Drupal\Tests\cloudflare_stream\Unit;

use Drupal\cloudflare_stream\Tus\UploadClient;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

/**
 * Test description.
 *
 * @group cloudflare_stream
 */
final class CloudflareStreamUploadClientTest extends UnitTestCase {

  /**
   * Instantiated class under test.
   */
  protected UploadClient $client;

  /**
   * Array to hold response history.
   *
   * @var array
   */
  protected array $responses = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // We simulate video upload by combining a small GIF and small chunk size.
    $this->client = new UploadClient(
      baseUrl: 'https://cloudflare.example.com/files',
      filePath: dirname(__DIR__, 2) . '/data/test-data.gif',
      apiToken: 'test-token',
      logger: $this->createMock('\Psr\Log\LoggerInterface'),
      chunkSize: 500,
    );
  }

  /**
   * Tests successful multi-chunk upload.
   */
  public function testMultiChunkUpload(): void {
    // Clear history.
    $this->responses = [];
    // Create the queue of expected responses.
    $expectedResponses = [];
    $expectedResponses[] = $this->buildResponse(204, 500);
    $expectedResponses[] = $this->buildResponse(204, 1000);
    $expectedResponses[] = $this->buildResponse(204, 1500);
    $expectedResponses[] = $this->buildResponse(204, 1972);
    $this->client->setHttpClient(
      $this->getClient($expectedResponses)
    );
    $this->client->upload();
    $this->assertCount(4, $this->responses);
  }

  /**
   * Tests successful multi-chunk upload.
   */
  public function testMultiChunkIntermittentFailures(): void {
    // Clear history.
    $this->responses = [];
    // Create the queue of expected responses.
    $expectedResponses = [];
    $expectedResponses[] = $this->buildResponse(204, 500);
    $expectedResponses[] = $this->buildResponse(204, 1000);
    $expectedResponses[] = $this->buildResponse(503);
    $expectedResponses[] = $this->buildResponse(204, 1500);
    $expectedResponses[] = $this->buildResponse(408);
    $expectedResponses[] = $this->buildResponse(204, 1972);
    $this->client->setHttpClient(
      $this->getClient($expectedResponses)
    );
    $this->client->upload();
    $this->assertCount(6, $this->responses);
  }

  /**
   * Helper function to build a mock Guzzle client.
   *
   * @param array $responseQueue
   *   An array of Response objects to return.
   *
   * @return \GuzzleHttp\Client
   *   The configured client.
   */
  protected function getClient(array $responseQueue = []): Client {
    $handler = new MockHandler($responseQueue);
    $handlerStack = HandlerStack::create($handler);

    // Add a history tracker.
    $history = Middleware::history($this->responses);
    $handlerStack->push($history);

    $client = new Client([
      'handler' => $handlerStack,
    ]);
    return $client;
  }

  /**
   * Including as a method so test plugins can override.
   *
   * @param int $code
   *   The HTTP code to return.
   * @param int $offset
   *   The offset to report back.
   * @param array $body
   *   The body (normally empty).
   *
   * @return \GuzzleHttp\Psr7\Response
   *   A prepared Oauth2 token response.
   */
  protected function buildResponse(int $code, int $offset = 0, array $body = []): Response {

    // Assemble the mock client.
    $body = json_encode($body);
    $headers = [];
    if ($code === 204) {
      $headers = [
        'Content-Type' => 'application/json',
        'Upload-Offset' => $offset,
      ];
    }
    // Match value to expected types in Response constructor.
    $body = $body === FALSE ? NULL : $body;
    return new Response(
      status: $code,
      headers: $headers,
      body: $body
    );
  }

}
